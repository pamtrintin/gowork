package com.mastertech.itau.gowork.models;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class UsuarioTest {
	
	@Test
	public void deveInicializarCorretamente(){
		Usuario usuario = new Usuario();
		
		usuario.setUsername("manu");
		assertEquals(usuario.getUsername(), "manu");
		
		usuario.setNome("Manuela");
		assertEquals(usuario.getNome(), "Manuela");
		
		usuario.setEmail("manuela@gmail.com");
		assertEquals(usuario.getEmail(), "manuela@gmail.com");
		
		usuario.setCelular("11987654321");
		assertEquals(usuario.getCelular(), "11987654321");
		
		usuario.setPassword("0011");
		assertEquals(usuario.getPassword(), "0011");

	}
}
