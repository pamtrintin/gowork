package com.mastertech.itau.gowork.models;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class RoteiroTest {

	@Test
	public void deveInicializarCorretamente(){
		Roteiro roteiro = new Roteiro();
		
		roteiro.setUsername("manu");
		assertEquals(roteiro.getUsername(), "manu");
		
		roteiro.setTrajetoInicial("Rua A 500");
		assertEquals(roteiro.getTrajetoInicial(), "Rua A 500");
		
		roteiro.setTrajetoFinal("Rua B 500");
		assertEquals(roteiro.getTrajetoFinal(), "Rua B 500");
	}
}
