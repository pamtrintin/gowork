package com.mastertech.itau.gowork.controllers;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mastertech.itau.gowork.models.Usuario;
import com.mastertech.itau.gowork.repositories.UsuarioRepository;
import com.mastertech.itau.gowork.services.JWTService;
import com.mastertech.itau.gowork.services.PasswordService;
import com.mastertech.itau.gowork.uteis.Perfis;

@RunWith(SpringRunner.class)
@WebMvcTest(UsuarioController.class)
public class UsuarioControllerTest {

	@MockBean
	UsuarioRepository usuarioRepository;
	
	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	JWTService jwtService;

	@MockBean
	PasswordService passwordService;
	
	ObjectMapper mapper = new ObjectMapper();
	
	@Test
	public void criarNovoUsuario() throws Exception{
		
		Usuario usuario = new Usuario();
				
		usuario.setUsername("manu");
		usuario.setNome("Manuela");
		usuario.setEmail("manuela@gmail.com");
		usuario.setCelular("11987654321");
		usuario.setPerfil(Perfis.MOTORISTA);
		usuario.setPassword("0011");
	
		when(usuarioRepository.save(any(Usuario.class))).thenReturn(usuario);
				
		String json = mapper.writeValueAsString(usuario);
		
		mockMvc.perform(post("/usuario")
				.content(json)
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(jsonPath("$.username", equalTo("manu")))
		.andExpect(jsonPath("$.nome", equalTo("Manuela")))
		.andExpect(jsonPath("$.email", equalTo("manuela@gmail.com")))
		.andExpect(jsonPath("$.celular", equalTo("11987654321")))
		.andExpect(jsonPath("$.perfil", equalTo("MOTORISTA")))
		.andExpect(jsonPath("$.password", equalTo("0011")));
	}
	
	@Test
	public void buscarUsuarios() throws Exception{
		
		Usuario usuario1 = new Usuario();
				
		usuario1.setUsername("manu");
		usuario1.setNome("Manuela");
		usuario1.setEmail("manuela@gmail.com");
		usuario1.setCelular("11987654321");
		usuario1.setPerfil(Perfis.MOTORISTA);
		usuario1.setPassword("0011");
	
		Usuario usuario2 = new Usuario();
		
		usuario2.setUsername("jose");
		usuario2.setNome("Jose");
		usuario2.setEmail("jose@gmail.com");
		usuario2.setCelular("11987654321");
		usuario2.setPerfil(Perfis.CARONA);
		usuario2.setPassword("1111");
		
		ArrayList<Usuario> usuarios = new ArrayList();
		usuarios.add(usuario1);
		usuarios.add(usuario2);
				
		when(usuarioRepository.findAll()).thenReturn(usuarios);
		
		mockMvc.perform(get("/usuario"))
		.andExpect(jsonPath("$[0].username", equalTo("manu")))
		.andExpect(jsonPath("$[0].nome", equalTo("Manuela")))
		.andExpect(jsonPath("$[0].email", equalTo("manuela@gmail.com")))
		.andExpect(jsonPath("$[0].celular", equalTo("11987654321")))
		.andExpect(jsonPath("$[0].perfil", equalTo("MOTORISTA")))
		.andExpect(jsonPath("$[0].password", equalTo("0011")))
		.andExpect(jsonPath("$[1].username", equalTo("jose")))
		.andExpect(jsonPath("$[1].nome", equalTo("Jose")))
		.andExpect(jsonPath("$[1].email", equalTo("jose@gmail.com")))
		.andExpect(jsonPath("$[1].celular", equalTo("11987654321")))
		.andExpect(jsonPath("$[1].perfil", equalTo("CARONA")))
		.andExpect(jsonPath("$[1].password", equalTo("1111")));
	}	
}
