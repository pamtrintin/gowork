const botaoCadastrar = document.querySelector("#cadastrar");

botaoCadastrar.onclick = efetuarCadastro;

function efetuarCadastro(){
    var username = document.querySelector("#username").value
    var nome = document.querySelector("#nome").value
    var email = document.querySelector("#email").value
    var celular = document.querySelector("#celular").value
    var senha = document.querySelector("#senha").value
    var perfil = document.querySelector("#perfil:checked").value
    var trajetoInicial = document.querySelector("#trajetoInicial").value
    var trajetoFinal = document.querySelector("#trajetoFinal").value
    
    if(perfil == 'M'){
        perfil = "MOTORISTA";
    } else {
        perfil = "CARONA";
    }
    var objUsuario = {"username":username, "nome":nome,  "email":email,  "celular":celular, "password":senha,  "perfil":perfil};
    var objRoteiro = {"username":username, "trajetoInicial":trajetoInicial,  "trajetoFinal":trajetoFinal};
    post('/usuario', objUsuario).then(function(dados){
    });   
    post('/roteiro', objRoteiro).then(function(dados){
    });  
}

function post(url, data){
    return fetch(url, {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-type': 'application/json'
        }
    })
    .then(function(response) {
    	if (response.status == 200) {
    		window.location = "/mapa";
    	}
        return response.json();
    })
    .catch(function(err) { 
        console.error(err);
        return {};
    });
}

