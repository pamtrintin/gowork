package com.mastertech.itau.gowork.models;

import java.time.LocalTime;
import java.util.UUID;

import javax.validation.constraints.NotNull;

import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.core.mapping.Table;

@Table
public class Roteiro {

	@PrimaryKeyColumn(type=PrimaryKeyType.PARTITIONED)
	private UUID idRoteiro;
	
	@NotNull
	private String username;
	
	@NotNull
	private String trajetoInicial;
	
	@PrimaryKeyColumn(type=PrimaryKeyType.CLUSTERED)
	private String trajetoFinal;
	
	private LocalTime horario;

	public UUID getIdRoteiro() {
		return idRoteiro;
	}

	public void setIdRoteiro(UUID idRoteiro) {
		this.idRoteiro = idRoteiro;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getTrajetoInicial() {
		return trajetoInicial;
	}

	public void setTrajetoInicial(String trajetoInicial) {
		this.trajetoInicial = trajetoInicial;
	}

	public String getTrajetoFinal() {
		return trajetoFinal;
	}

	public void setTrajetoFinal(String trajetoFinal) {
		this.trajetoFinal = trajetoFinal;
	}

	public LocalTime getHorario() {
		return horario;
	}

	public void setHorario(LocalTime horario) {
		this.horario = horario;
	}
}
