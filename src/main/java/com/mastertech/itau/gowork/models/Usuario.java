package com.mastertech.itau.gowork.models;

import javax.validation.constraints.NotNull;

import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

import com.mastertech.itau.gowork.uteis.Perfis;

@Table
public class Usuario {

	@PrimaryKey
	private String username;

	@NotNull
	private String nome;

	@NotNull
	private String email;

	@NotNull
	private String celular;

	@NotNull
	private String password;

	@NotNull
	private Perfis perfil;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Perfis getPerfil() {
		return perfil;
	}

	public void setPerfil(Perfis perfil) {
		this.perfil = perfil;
	};

}
