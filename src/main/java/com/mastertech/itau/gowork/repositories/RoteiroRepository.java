package com.mastertech.itau.gowork.repositories;

import java.util.ArrayList;
import java.util.UUID;

import org.springframework.data.cassandra.repository.AllowFiltering;
import org.springframework.data.repository.CrudRepository;

import com.mastertech.itau.gowork.models.Roteiro;


public interface RoteiroRepository extends CrudRepository<Roteiro, UUID>{
	@AllowFiltering
	public ArrayList<Roteiro> findByTrajetoFinal(String trajetoFinal);

}
