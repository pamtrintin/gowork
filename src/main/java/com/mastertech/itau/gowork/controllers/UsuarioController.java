package com.mastertech.itau.gowork.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.mastertech.itau.gowork.models.Usuario;
import com.mastertech.itau.gowork.repositories.UsuarioRepository;
import com.mastertech.itau.gowork.services.JWTService;
import com.mastertech.itau.gowork.services.PasswordService;

@CrossOrigin
@RestController
public class UsuarioController {

	@Autowired
	JWTService jwtService;

	@Autowired
	PasswordService passwordService;

	@Autowired
	UsuarioRepository usuarioRepository;
	
	@RequestMapping(method = RequestMethod.POST, path = "/usuario")
	public Usuario criar(@RequestBody Usuario usuario) {
		String hash = passwordService.gerarHash(usuario.getPassword());
		usuario.setPassword(hash);
		return usuarioRepository.save(usuario);
	}

	@RequestMapping(method = RequestMethod.GET, path = "/usuario")
	public Iterable<?> buscar() {
		return usuarioRepository.findAll();
	}
	
	@RequestMapping("/usuario/{username}")
	public ResponseEntity<?> getUsuarioByName(@PathVariable String username) {
		Optional<Usuario> usuarios = usuarioRepository.findByUsername(username.toLowerCase());
		return ResponseEntity.ok(usuarios);
	}
	
	@RequestMapping(method = RequestMethod.POST, path = "/login")
	public ResponseEntity<?> fazerLogin(@RequestBody Usuario usuario) {
		Optional<Usuario> usuarioBancoOptional = usuarioRepository.findByUsername(usuario.getUsername());

		if (!usuarioBancoOptional.isPresent()) {
			return ResponseEntity.badRequest().build();
		}

		Usuario usuarioBanco = usuarioBancoOptional.get();

		if (passwordService.verificarHash(usuario.getPassword(), usuarioBanco.getPassword())) {
			String token = jwtService.gerarToken(usuarioBanco.getUsername());

			HttpHeaders headers = new HttpHeaders();
			headers.add("Authorization", String.format("Bearer %s", token));
			headers.add("Access-Control-Expose-Headers", "Authorization");

			return ResponseEntity.ok().body(usuarioBanco);
		}
		return ResponseEntity.badRequest().build();
	}
}
