package com.mastertech.itau.gowork.controllers;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.mastertech.itau.gowork.models.Roteiro;
import com.mastertech.itau.gowork.repositories.RoteiroRepository;
import com.mastertech.itau.gowork.repositories.UsuarioRepository;

@CrossOrigin
@RestController
@RequestMapping("/roteiro")
public class RoteiroController {
	
	@Autowired
	RoteiroRepository roteiroRepository;
	
	@Autowired
	UsuarioRepository usuarioRepository;
	
	
	@RequestMapping(method=RequestMethod.POST)
	public Roteiro criar(@RequestBody Roteiro roteiro) {
		roteiro.setIdRoteiro(UUID.randomUUID());
		roteiro.setTrajetoFinal(roteiro.getTrajetoFinal().toLowerCase());
		return roteiroRepository.save(roteiro);
	}
	
	@RequestMapping(method=RequestMethod.GET)
	public Iterable<?> buscar() {
		return roteiroRepository.findAll();
	}
	
	@RequestMapping("{trajetoFinal}")
	public ResponseEntity<?> getRoteiroByName(@PathVariable String trajetoFinal) {
		ArrayList<Roteiro> roteiros = roteiroRepository.findByTrajetoFinal(trajetoFinal.toLowerCase());
		return ResponseEntity.ok(roteiros);
		
	}



}



